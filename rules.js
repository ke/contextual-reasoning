function instanceOf_rule1($this) {
    var r1 = getProperty($this, "http://www.wikidata.org/prop/P31");
    var object = getProperty(r1, "http://www.wikidata.org/prop/statement/P31");
    var r2 = getProperty(object, "http://www.wikidata.org/prop/P279");
    var object1 = getProperty(r2, "http://www.wikidata.org/prop/statement/P279");

    var p31 = TermFactory.namedNode("http://www.wikidata.org/prop/P31");
    var ps31 = TermFactory.namedNode("http://www.wikidata.org/prop/statement/P31");

    var bn = TermFactory.blankNode();

    return [
        [$this, p31, bn],
        [bn, ps31, object1]
    ];
}

function instanceOf_rule2($this) {
    var r1 = getProperty($this, "http://www.wikidata.org/prop/P31");
    var object = getProperty(r1, "http://www.wikidata.org/prop/statement/P31");
    var r2 = getProperty(object, "http://www.wikidata.org/prop/P279");
    var object1 = getProperty(r2, "http://www.wikidata.org/prop/statement/P279");

    var r3 = getProperty($this, "http://www.wikidata.org/prop/P31");
    //var object1 = getProperty(r3, "http://www.wikidata.org/prop/statement/P31");

    var r1ValidityContext = getProperty(r1, "http://www.wikidata.org/prop/qualifier/validityContext");
    var r2ValidityContext = getProperty(r2, "http://www.wikidata.org/prop/qualifier/validityContext");

    var r1Causality = getProperty(r1, "http://www.wikidata.org/prop/qualifier/Causality");
    var r2Causality = getProperty(r2, "http://www.wikidata.org/prop/qualifier/Causality");

    var startDate = intersect(r1ValidityContext, r2ValidityContext);
    var endDate = intersect2(r1ValidityContext, r2ValidityContext);

    var pqValidityContext = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/validityContext");
    var causality = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/Causality");
    var p580 = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P580");
    var p582 = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P582");

    var p1828 = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/1828");
    var p1534 = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/1534");

    var hc1 = getProperty(r1Causality, "http://www.wikidata.org/prop/qualifier/1828");
    var hc2 = getProperty(r2Causality, "http://www.wikidata.org/prop/qualifier/1828");

    var ec1 = getProperty(r1Causality, "http://www.wikidata.org/prop/qualifier/1534");
    var ec2 = getProperty(r2Causality, "http://www.wikidata.org/prop/qualifier/1534");

    var bn = TermFactory.blankNode();
    var bn1 = TermFactory.blankNode();
    var bn2 = TermFactory.blankNode();
    var bn3 = TermFactory.blankNode();
    var bn4 = TermFactory.blankNode();
    var bn5 = TermFactory.blankNode();

    var type = TermFactory.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
    var list = TermFactory.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#list");
    var first = TermFactory.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#first");
    var rest = TermFactory.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#rest");
    var nil = TermFactory.namedNode("http://www.w3.org/1999/02/22-rdf-syntax-ns#nil");

    return [
        [r3, pqValidityContext, bn],
        [bn, p580, startDate],
        [bn, p582, endDate],
        [r3, causality, bn1],

        [bn1, p1828, bn2],
        [bn2, type, list],
        [bn2, first, hc1],
        [bn2, rest, bn3],
        [bn3, type, list],
        [bn3, first, hc2],
        [bn3, rest, nil],

        [bn1, p1534, bn4],
        [bn4, type, list],
        [bn4, first, ec1],
        [bn4, rest, bn3],
        [bn5, type, list],
        [bn5, first, ec2],
        [bn5, rest, nil]
    ];
}

function sequence_previous($this) {
    var triples = getAllTriples($this);
    var wds = null;
    for (var i = 0; i < triples.length; i++) {
        if (triples[i].object.toString().startsWith("http://www.wikidata.org/entity/statement/")) {
            wds = triples[i].object;
        }
    }
    if (!wds) {
        return;
    }

    var predicate = getPredicate($this, wds);
    var predicateS = predicate.toString();
    var p39 = predicateS.substring(predicateS.lastIndexOf("/") + 1);

    var ps39 = TermFactory.namedNode("http://www.wikidata.org/prop/statement/" + p39);
    var obj = getProperty(wds, ps39);

    var seq_auxnode = getProperty(wds, "http://www.wikidata.org/prop/qualifier/Sequence");
    var replaces = getProperty(seq_auxnode, "http://www.wikidata.org/prop/qualifier/P1365");
    var replacedBy = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P1366");

    if (!replaces) {
        return;
    }

    var vc_auxnode = getProperty(wds, "http://www.wikidata.org/prop/qualifier/validityContext");
    var startDate = getProperty(vc_auxnode, "http://www.wikidata.org/prop/qualifier/P580");
    var endTimeProperty = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P582");

    var bn = TermFactory.blankNode();
    return [
        [replaces, predicate, bn],
        [bn, ps39, obj],
        [bn, replacedBy, $this],
        [bn, endTimeProperty, startDate]
    ];
}

function sequence_next($this) {
    var triples = getAllTriples($this);
    var wds = null;
    for (var i = 0; i < triples.length; i++) {
        if (triples[i].object.toString().startsWith("http://www.wikidata.org/entity/statement/")) {
            wds = triples[i].object;
        }
    }
    if (!wds) {
        return;
    }

    var predicate = getPredicate($this, wds);
    var predicateS = predicate.toString();
    var p39 = predicateS.substring(predicateS.lastIndexOf("/") + 1);

    var ps39 = TermFactory.namedNode("http://www.wikidata.org/prop/statement/" + p39);
    var obj = getProperty(wds, ps39);

    var seq_auxnode = getProperty(wds, "http://www.wikidata.org/prop/qualifier/Sequence");
    var replacedBy = getProperty(seq_auxnode, "http://www.wikidata.org/prop/qualifier/P1366");
    var replaces = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P1365");

    if (!replacedBy) {
        return;
    }

    var vc_auxnode = getProperty(wds, "http://www.wikidata.org/prop/qualifier/validityContext");
    var endTime = getProperty(vc_auxnode, "http://www.wikidata.org/prop/qualifier/P582");
    var startTimeProperty = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P580");

    var bn = TermFactory.blankNode();
    return [
        [replacedBy, predicate, bn],
        [bn, ps39, obj],
        [bn, replaces, $this],
        [bn, startTimeProperty, endTime]
    ];
}

function intersect_rule(vc1) {
    var wds1 = getProperty(vc1, "http://www.wikidata.org/prop/P31");
    var vcnode1 = getProperty(wds1, "http://www.wikidata.org/prop/qualifier/validityContext");
    var startDate1 = getProperty(vcnode1, "http://www.wikidata.org/prop/qualifier/P580");
    var endDate1 = getProperty(vcnode1, "http://www.wikidata.org/prop/qualifier/P582");

    var obj = getProperty(wds1, "http://www.wikidata.org/prop/statement/P31");
    var wds2 = getProperty(obj, "http://www.wikidata.org/prop/P279");
    var vcnode2 = getProperty(wds2, "http://www.wikidata.org/prop/qualifier/validityContext");
    var startDate2 = getProperty(vcnode2, "http://www.wikidata.org/prop/qualifier/P580");
    var endDate2 = getProperty(vcnode2, "http://www.wikidata.org/prop/qualifier/P582");

    var startDate = Math.min(startDate1.lex, startDate2.lex);
    var endDate = Math.max(endDate1.lex, endDate2.lex);

    var tmp1 = startDate1.lex.split("-")[0];
    var tmp2 = startDate2.lex.split("-")[0];

    var tmp3 = endDate1.lex.split("-")[0];
    var tmp4 = endDate2.lex.split("-")[0];

    var startDate = TermFactory.literal(Math.max(tmp1, tmp2), "http://www.w3.org/2001/XMLSchema#integer");
    var endDate = TermFactory.literal(Math.min(tmp3, tmp4), "http://www.w3.org/2001/XMLSchema#integer");

    var bn = TermFactory.blankNode();

    return [
        [bn, TermFactory.namedNode("startDate"), startDate],
        [bn, TermFactory.namedNode("endDate"), endDate],
    ];
}

function symmetric($this) {
    var sub = getSubject(null, $this);
    var p26Property = getPredicate(sub, $this);
    var p26 = p26Property.toString().substring(p26Property.toString().lastIndexOf("/") + 1);

    var ps26 = TermFactory.namedNode("http://www.wikidata.org/prop/statement/" + p26);
    var obj = getProperty($this, ps26);

    var vc_auxnode = getProperty($this, "http://www.wikidata.org/prop/qualifier/validityContext");
    var startDate = getProperty(vc_auxnode, "http://www.wikidata.org/prop/qualifier/P580");
    var endDate = getProperty(vc_auxnode, "http://www.wikidata.org/prop/qualifier/P582");

    var p580 = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P580");
    var p582 = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/P582");

    var pqValidityContext = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/validityContext");
    var causality = TermFactory.namedNode("http://www.wikidata.org/prop/qualifier/causality");

    var c_auxnode = getProperty($this, "http://www.wikidata.org/prop/qualifier/causality");
    var causalities = getPropertyObject(c_auxnode);

    var bn = TermFactory.blankNode();
    var vc_node = TermFactory.blankNode();
    var c_node = TermFactory.blankNode();

    var ret = [];

    ret.push([obj, p26Property, bn],
        [bn, ps26, sub],
        [bn, pqValidityContext, vc_node],
        [vc_node, p580, startDate],
        [vc_node, p582, endDate],
        [bn, causality, c_node]
    );

    for (var i = 0; i < causalities.length; i++) {
        ret.push([c_node, causalities[i].predicate, causalities[i].object]);
    }

    return ret;
}

function intersect(vc1, vc2) {
    var startDate1 = getProperty(vc1, "http://www.wikidata.org/prop/qualifier/P580");
    var startDate2 = getProperty(vc2, "http://www.wikidata.org/prop/qualifier/P580");

    var startDate = Math.min(startDate1.lex, startDate2.lex);

    var tmp1 = startDate1.lex.split("-")[0];
    var tmp2 = startDate2.lex.split("-")[0];

    var startDate = TermFactory.literal(Math.max(tmp1, tmp2), "http://www.w3.org/2001/XMLSchema#integer");

    return startDate;
}

function intersect2(vc1, vc2) {
    var endDate1 = getProperty(vc1, "http://www.wikidata.org/prop/qualifier/P582");
    var endDate2 = getProperty(vc2, "http://www.wikidata.org/prop/qualifier/P582");

    var endDate = Math.max(endDate1.lex, endDate2.lex);

    var tmp3 = endDate1.lex.split("-")[0];
    var tmp4 = endDate2.lex.split("-")[0];

    var endDate = TermFactory.literal(Math.min(tmp3, tmp4), "http://www.w3.org/2001/XMLSchema#integer");

    return endDate;
}

function union_causality($causalityNode) {
    var cause1 = getProperty($causalityNode, "http://www.wikidata.org/prop/qualifier/1534");
    return cause1;
}

function union_provenance(prov) {
    var wds1 = getProperty(prov, "http://www.wikidata.org/prop/P31");
    var all1 = getAllPropertyObject(wds1, "http://www.w3.org/ns/prov#wasDerivedFrom");

    var obj = getProperty(wds1, "http://www.wikidata.org/prop/statement/P31");
    var wds2 = getProperty(obj, "http://www.wikidata.org/prop/P279");
    var all2 = getAllPropertyObject(wds2, "http://www.w3.org/ns/prov#wasDerivedFrom");

    return all1;

    // TODO return union of all1 and all2
}

function union_provenance2(prov) {
    var wds1 = getProperty(prov1, "http://www.wikidata.org/prop/P31");
    var all1 = getAllPropertyObject(wds1, "http://www.w3.org/ns/prov#wasDerivedFrom");

    var obj = getProperty(wds1, "http://www.wikidata.org/prop/statement/P31");
    var wds2 = getProperty(obj, "http://www.wikidata.org/prop/P279");
    var all2 = getAllPropertyObject(wds2, "http://www.w3.org/ns/prov#wasDerivedFrom");

    return all2;

    // TODO return union of all1 and all2
}


function getProperty(obj, name) {
    var it = $data.find(obj, TermFactory.namedNode(name), null);
    var result = it.next().object;
    it.close();
    return result;
}

function getPredicate(sub, obj) {
    // TODO to check
    var it = $data.find(sub, null, obj);
    var result = it.next().predicate;
    it.close();
    return result;
}

function getSubject(pred, obj) {
    var it = $data.find(null, pred, obj);
    var result = it.next().subject;
    it.close();
    return result;
}

function getAllTriples(obj) {
    var triples = [];
    var it = $data.find(obj, null, null);
    for (var t = it.next(); t; t = it.next()) {
        triples.push(t);
    }
    it.close();
    return triples;
}

function getAllPropertyObject(obj, name) {
    var props = [];
    var it = $data.find(obj, TermFactory.namedNode(name), null);
    for (var prop = it.next(); prop; prop = it.next()) {
        props.push(prop.object);
    }
    it.close();
    return props;
}

function getPropertyObject(sub) {
    var props = [];
    var it = $data.find(sub, null, null);
    for (var prop = it.next(); prop; prop = it.next()) {
        props.push({
            predicate: prop.predicate,
            object: prop.object
        });
    }
    it.close();
    return props;
}
